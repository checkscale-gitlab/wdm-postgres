import json
import os

import pika

credentials = pika.PlainCredentials(
    username=os.environ.get('RABBITMQ_USERNAME'),
    password=os.environ.get('RABBITMQ_PASSWORD')
)

params = pika.ConnectionParameters(
    host=os.environ.get('RABBITMQ_HOST'),
    port=os.environ.get('RABBITMQ_PORT'),
    credentials=credentials,
    heartbeat=600,
    blocked_connection_timeout=300
)

connection = pika.BlockingConnection(params)
channel = connection.channel()


def publish_order_queue(method, body):
    properties = pika.BasicProperties(method)
    channel.basic_publish(exchange='', routing_key='order_items_created', body=json.dumps(body), properties=properties)
