from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.response import Response

from .models import Item, Order, OrderItemMembership, User
from .rpc import RPC
from .serializers import OrderSerializer


class OrderViewSet(viewsets.ViewSet):
    """
    POST - creates an order for the given user, and returns an order_id
    Output JSON fields:
        “order_id”  - the order’s id
    """

    def create(self, request, user_id=None):
        user = get_object_or_404(User, pk=int(user_id))

        try:
            order = Order(user_id=user.id, is_paid=False)
            order.save()
            return JsonResponse({'order_id': order.id}, status=200)

        except Exception as e:
            # print(e)
            return Response(status=400)

    """
    DELETE - deletes and order by ID
    """

    def remove(self, request, order_id=None):
        order = get_object_or_404(Order, pk=int(order_id))

        try:
            order.delete()
            return Response(status=200)

        except Exception as e:
            # print(e)
            return Response(status=400)

    """
    GET - retrieves the information of an order (id, payment status, items included and user id)
    Output JSON fields:
        “order_id”  - the order’s id
        “paid” - (true/false)
        “items”  - list of item ids that are included in the order
        “user_id”  - the user’s id that made the order
        “total_cost” - the total cost of the items in the order
    """

    def find(self, request, order_id):
        order = get_object_or_404(Order, pk=int(order_id))
        serializer = OrderSerializer(instance=order)
        return Response(serializer.data, status=200)

    """
    POST - adds a given item in the order given
    """

    def add_item(self, request, order_id, item_id):
        item = get_object_or_404(Item, pk=int(item_id))
        order = get_object_or_404(Order, pk=int(order_id))

        if OrderItemMembership.objects.filter(order_id=order.id, item_id=item.id).exists():
            order_item_membership = OrderItemMembership.objects.get(order_id=order.id, item_id=item.id)
            order_item_membership.count += 1
            order_item_membership.save()

        order.items.add(item)
        order.save()

        return Response(status=200)

    """
    DELETE - removes the given item from the given order
    """

    def remove_item(self, request, order_id, item_id):
        get_object_or_404(Order, pk=int(order_id))
        order_item_membership = OrderItemMembership.objects.get(order_id=int(order_id), item_id=int(item_id))

        if order_item_membership is not None:
            order_item_membership.count -= 1
            order_item_membership.save()

            if order_item_membership.count <= 0:
                order_item_membership.delete()

        return Response(status=200)

    """
    POST - makes the payment (via calling the payment service), 
        subtracts the stock (via the stock service) and returns a status (success/failure).
    """

    def checkout(self, request, order_id):
        order = get_object_or_404(Order, pk=int(order_id))
        if order.is_paid:
            return Response(status=400)

        original_items = order.items.all()
        item_counts = []
        total_cost = 0

        order_serializer = OrderSerializer(order)
        data = order_serializer.data

        for item in original_items:
            order_item_membership = OrderItemMembership.objects.get(order_id=order.id, item_id=item.id)
            total_cost += item.cost * order_item_membership.count
            item_counts.append(order_item_membership.count)

        data['total_cost'] = total_cost
        data['item_counts'] = item_counts
        data['order_id'] = order.id

        payment_rpc = RPC()
        response = payment_rpc.call_payment_service('checkout', data)
        
        if response['content_type'] == 'success':
            order.is_paid = True
            order.save()

            return Response(status=200)
        else:
            order.is_paid = False
            order.save()

            return Response(status=400)
